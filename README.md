This Newswire WordPress Plugin is a professional news distribution and syndication tool. It has important features that allow news outlets to automatically publish trusted, high-quality, ready-to-publish news content on WordPress.

Previously, WordPress news website publishers needed to either manually copy and paste each news item they wanted to publish, or code complex and expensive software. But this plugin makes it simple for a subscriber to automatically receive and easily publish reputable and accurate news content from the Fourth Estate newswire.

The Newswire WordPress Publisher plugin dramatically simplifies the process of receiving news data feeds from the Fourth Estate — or other compatible newswires. Once automatically received, the news content and images are posted to your news website, or they can be held as drafts for manual approval and publication.

- News Content is received and automatically updated using HTTP-Push streaming
- Multi-format support: Text, pictures, graphics, video, and audio are all supported when available
- Integrates with any WordPress theme
- Import news article keywords and news sluglines as WP tags
- Ability to import IAB categories or IPTC MediaTopics as WordPress categories
- Many viewing options available including Integration with Press Release View module
- Content be automatically published into WordPress immediately when received, or it can be held as a draft for review prior to publication    


# What is the Newswire Publisher plugin?
The Newswire Wordpress Publisher plugin enables content to be automatically sent from the Fourth Estate's (www.fourthestate.org) newsroom management system to a subscriber's WordPress website.

**NOTE: ** In order to use this plugin, you must have an active [Fourth Estate](https://www.fourthestate.org) Newswire Account. This is a paid subscription and details for obtaining these credentials will be provided by your Fourth Estate represenative.


# What does the WordPress Newswire Publisher plugin do?
When paired with an active Fourth Estate Newswire subscription, and a properly configured WordPress Newswire Publisher plugin, the news content is automatically published to WordPress. Users can still log in to WordPress to administer the site and content as they normally would.

# How does it work?
Fourth Estate's news content management system supports the IPTC's industry standard ninjs (http://dev.iptc.org/ninjs), which standardizes the representation of news content in JSON - a lightweight, easy-to-parse, data interchange format. The Newswire Publisher plugin will also enable multiple authorized instances of WordPress to receive content from the Fourth Estate newswire, so you could have multiple, or only one, or none at all (which wouldn't be that useful but still possible (wink)).

This plugin should also theoretially work with other news and information providers that are based on the Superdesk (www.superdesk.org) platform, or that use ninjs.

The Newswire Publisher plugin automatically receives news and content via HTTP PUSH and parses the content in ninjs. When the plugin receives new content via HTTP PUSH, it automatically loads this content into the WordPress database via the REST API.

# How to Install the plugin?

1.  Install through the WordPress directory or download, unzip and upload the files to your `/wp-content/plugins/` directory.
2.  Activate the plugin through the 'Plugins' menu in WordPress.
3.  Contact your Fourth Estate Newswire representative and provide them the 'Autoload' URL located in the plugin settings menu.
4.  Configure settings in the 'Tools' menu according to instructions you will receive from your Fourth Estate Newswire representative.

# Settings menu
A settings menu in WordPress lets you customize the way WordPress handles content from the Fourth Estate Newswire.

For example, you can map categories to WordPress's tags, or use newswire subject codes as categories. Authors, copyright information, post status, and default categories are just some of the WordPress fields that can be configured from the Settings menu.

# How do we signup for a Fourth Estate newswire subscription?
The Fourth Estate Newswire (www.fourthestate.org/newswire/) provides Ready-to-publish news content delivered to you as the news unfolds. All content is original - created by a global team of journalists, producers, and photographers.

# Help! I need support.
Technical and implementation support is provided for free to Fourth Estate Newswire Subscribers, and available as a paid professional service to non-subscribers.
