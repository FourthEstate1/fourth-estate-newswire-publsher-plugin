=== Fourth Estate Newswire Publisher ===
Contributors: FourthEstate, fourthestatetech
Donate link: http://fourthestate.org/
Tags: Fourth Estate, newswire, news article, media, photos, Super Desk
Requires at least: 4.7
Tested up to: 5.7
Stable tag: 1.0
Version: 1.0
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Newswire WordPress Publisher plugin enables news to be automatically sent from the Fourth Estate newswire to a subscriber's WordPress site.

== Description ==

The Newswire WordPress Publisher plugin enables content to be automatically sent from the Fourth Estate's (www.fourthestate.org) newsroom management system to a subscriber's WordPress instance it's installed on.

When paired with an active Fourth Estate Newswire subscription, and a properly configured WordPress Newswire Publisher plugin, the news content is automatically published to WordPress.

You can still log in to WordPress to administer the site and content as you normally would.

*** In order to use this plugin, you must have a [Fourth Estate](http://fourthestate.org/ "Fourth Estate") Newswire Account. This is a paid subscription and details for obtaining these credentials can be found in the FAQ. ***

== Installation ==

1.  Install through the WordPress directory or download, unzip and upload the files to your `/wp-content/plugins/` directory.
2.  Activate the plugin through the 'Plugins' menu in WordPress.
3.  Contact your Fourth Estate Newswire representative and provide them the 'Autoload' URL located in the plugin settings menu.
4.  Configure settings in the 'Tools' menu according to instructions you will receive from your Fourth Estate Newswire representative.

== Frequently Asked Questions ==

= Is this an official Fourth Estate plugin? =

Yes, is an official Fourth Estate plugin and approved news content delivery method.

= How do I obtain a Fourth Estate Newswire account and credentials? =

You may obtain request access to the Fourth Estate Newswire by contacting [Fourth Estate](http://fourthestate.org/ "Fourth Estate") sales. An account representative will contact you to set up your account.

= Can we change the allowed attributes and tags? =

A settings menu in WordPress lets you customize the way WordPress handles incoming news content from the Fourth Estate Newswire.

For example, you can map categories to WordPress's tags, or use newswire subject codes as categories. Authors, copyright information, post status, and default categories are just some of the WordPress fields that can be configured from the Settings menu.

Note: the plugin maintains a log file at /newswire-wordpress-plugin/log/newswire.txt that should be manually truncated from time-to-time.

== Screenshots ==

1. Plugin settings page.

== Changelog ==

= 1.0.0 =
* Initial Release


== Upgrade Notice ==

= 1.0.0 =
* Initial Public Release

= 0.9.9.9.9 =
* Bela Release Candidate

== Credits ==

Created and maintained by the [Fourth Estate](https://www.fourthestate.org). Based upon [Superdesk](https://www.superdesk.org) WordPress plugin, which was authored by the [European Journalism Centre](https://ejc.net/) and the financial news outlet 'The Source'.
